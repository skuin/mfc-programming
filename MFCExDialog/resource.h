﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MFCExDialog.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MFCEXDIALOG_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_ENLIST               1000
#define IDC_EDIT_NAME                   1001
#define IDC_BUTTON_DELETE               1002
#define IDC_RADIO_FEMALE                1003
#define IDC_RADIO_MALE                  1004
#define IDC_CHECK_APP                   1005
#define IDC_CHECK_CLOUD                 1006
#define IDC_CHECK_EMB                   1007
#define IDC_CHECK_NET                   1008
#define IDC_LIST_MEMBER                 1009
#define IDC_COMBO1                      1011
#define IDC_COMBO_YEAR                  1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
