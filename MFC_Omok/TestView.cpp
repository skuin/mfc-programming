﻿// TestView.cpp: 구현 파일
//

#include "stdafx.h"
#include "MFC_Omok.h"
#include "TestView.h"


// Test

IMPLEMENT_DYNCREATE(Test, CView)

Test::Test()
{

}

Test::~Test()
{
}

BEGIN_MESSAGE_MAP(Test, CView)
END_MESSAGE_MAP()


// Test 그리기

void Test::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: 여기에 그리기 코드를 추가합니다.
}


// Test 진단

#ifdef _DEBUG
void Test::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void Test::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// Test 메시지 처리기
