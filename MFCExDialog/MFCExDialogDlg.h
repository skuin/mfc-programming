﻿
// MFCExDialogDlg.h: 헤더 파일
//

#pragma once


// CMFCExDialogDlg 대화 상자
class CMFCExDialogDlg : public CDialogEx
{
// 생성입니다.
public:
	CMFCExDialogDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCEXDIALOG_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditName();
	CString m_strName;
	CString m_strInfo;
	afx_msg void OnBnClickedButtonEnlist();
	CButton m_App;
	CButton m_Net;
	CButton m_Cloud;
	CButton m_Emb;
	int m_Radio = 1;
	CComboBox m_Year;
	CListBox m_Member;
	afx_msg void OnBnClickedButtonDelete();
	afx_msg void OnSelchangeComboYear();
	int m_nSelectedItem;
	int m_nAge;
	afx_msg void OnBnClickedRadioFemale();
	afx_msg void OnBnClickedRadioMale();
};
