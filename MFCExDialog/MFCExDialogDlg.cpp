﻿
// MFCExDialogDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "MFCExDialog.h"
#include "MFCExDialogDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCExDialogDlg 대화 상자



CMFCExDialogDlg::CMFCExDialogDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCEXDIALOG_DIALOG, pParent)
	, m_strName(_T(""))
	, m_strInfo(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCExDialogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NAME, m_strName);
	DDX_Control(pDX, IDC_CHECK_APP, m_App);
	DDX_Control(pDX, IDC_CHECK_NET, m_Net);
	DDX_Control(pDX, IDC_CHECK_CLOUD, m_Cloud);
	DDX_Control(pDX, IDC_CHECK_EMB, m_Emb);
	DDX_Control(pDX, IDC_COMBO_YEAR, m_Year);
	DDX_Control(pDX, IDC_LIST_MEMBER, m_Member);
}

BEGIN_MESSAGE_MAP(CMFCExDialogDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_EDIT_NAME, &CMFCExDialogDlg::OnEnChangeEditName)
	ON_BN_CLICKED(IDC_BUTTON_ENLIST, &CMFCExDialogDlg::OnBnClickedButtonEnlist)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, &CMFCExDialogDlg::OnBnClickedButtonDelete)
	ON_CBN_SELCHANGE(IDC_COMBO_YEAR, &CMFCExDialogDlg::OnSelchangeComboYear)
	ON_BN_CLICKED(IDC_RADIO_FEMALE, &CMFCExDialogDlg::OnBnClickedRadioFemale)
	ON_BN_CLICKED(IDC_RADIO_MALE, &CMFCExDialogDlg::OnBnClickedRadioMale)
END_MESSAGE_MAP()


// CMFCExDialogDlg 메시지 처리기

BOOL CMFCExDialogDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	int i;
	CString year;
	for (i = 1960; i <= 2050; i++)
	{
		year.Format(_T("%d"), i);
		m_Year.AddString(year);
	}
	m_Year.SetCurSel(0);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMFCExDialogDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMFCExDialogDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMFCExDialogDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCExDialogDlg::OnEnChangeEditName()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMFCExDialogDlg::OnBnClickedButtonEnlist()
{
	LPCTSTR gender[2] = { _T("여성"), _T("남성") };
	BOOL bCheck;
	UpdateData(TRUE);
	if (m_strName.IsEmpty())
		return;
	m_strInfo.Append(m_strName);
	m_strInfo.Append(_T(" : "));
	m_strInfo.Append(gender[m_Radio]);
	m_strInfo.Append(_T(" : "));
	if (m_nAge > 0)
		m_strInfo.AppendFormat(_T("%d"), m_nAge);

	bCheck = m_App.GetCheck();
	if (bCheck)
	{
		m_strInfo.Append(_T(", 앱 개발"));
		m_App.SetCheck(0);
	}
	bCheck = m_Net.GetCheck();
	if (bCheck)
	{
		m_strInfo.Append(_T(", 네트워크 보안"));
		m_Net.SetCheck(0);
	}
	bCheck = m_Cloud.GetCheck();
	if (bCheck)
	{
		m_strInfo.Append(_T(", 클라우드"));
		m_Cloud.SetCheck(0);
	}
	bCheck = m_Emb.GetCheck();
	if (bCheck)
	{
		m_strInfo.Append(_T(", 엠비디드"));
		m_Emb.SetCheck(0);
	}
	m_Member.AddString(m_strInfo);
	m_strInfo.Empty();
	m_strName.Empty();
	m_Year.SetCurSel(0);
	m_nAge = 0;
	UpdateData(FALSE);
}


void CMFCExDialogDlg::OnBnClickedButtonDelete()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_nSelectedItem = m_Member.GetCurSel();
	m_Member.DeleteString(m_nSelectedItem);
	m_nSelectedItem = -1;
	UpdateData(FALSE);
}


void CMFCExDialogDlg::OnSelchangeComboYear()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CTime time;
	CString temp;
	int curYear;
	time = CTime::GetCurrentTime();
	this->m_Year.GetLBText(this->m_Year.GetCurSel(), temp);
	curYear = _tstoi(temp);
	m_nAge = time.GetYear() - curYear + 1;
}


void CMFCExDialogDlg::OnBnClickedRadioFemale()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Radio = 0;
}


void CMFCExDialogDlg::OnBnClickedRadioMale()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_Radio = 1;

}
