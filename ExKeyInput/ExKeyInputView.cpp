﻿
// ExKeyInputView.cpp: CExKeyInputView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "ExKeyInput.h"
#endif

#include "ExKeyInputDoc.h"
#include "ExKeyInputView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExKeyInputView

IMPLEMENT_DYNCREATE(CExKeyInputView, CView)

BEGIN_MESSAGE_MAP(CExKeyInputView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CHAR()
END_MESSAGE_MAP()

// CExKeyInputView 생성/소멸

CExKeyInputView::CExKeyInputView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CExKeyInputView::~CExKeyInputView()
{
}

BOOL CExKeyInputView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CExKeyInputView 그리기

void CExKeyInputView::OnDraw(CDC* pDC)
{
	CExKeyInputDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
	pDC->TextOut(0, 0, m_str, m_str.GetLength());
}


// CExKeyInputView 인쇄

BOOL CExKeyInputView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CExKeyInputView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CExKeyInputView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CExKeyInputView 진단

#ifdef _DEBUG
void CExKeyInputView::AssertValid() const
{
	CView::AssertValid();
}

void CExKeyInputView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CExKeyInputDoc* CExKeyInputView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CExKeyInputDoc)));
	return (CExKeyInputDoc*)m_pDocument;
}
#endif //_DEBUG


// CExKeyInputView 메시지 처리기


void CExKeyInputView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_BACK)
		m_str.Delete(m_str.GetLength() - 1);
	else
		m_str.AppendChar(nChar);
	Invalidate();

	CView::OnChar(nChar, nRepCnt, nFlags);
}
