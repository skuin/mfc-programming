﻿#pragma once

// CChildSock 명령 대상

class CChildSock : public CAsyncSocket
{
public:
	CChildSock();
	virtual ~CChildSock();
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
};


