﻿
// MFC_OmokDlg.h: 헤더 파일
//

#pragma once
#include "TestView.h"

enum BADUKDOL {NONE, BLACK, WHITE};

// CMFCOmokDlg 대화 상자
class CMFCOmokDlg : public CDialogEx
{
// 생성입니다.
public:
	CMFCOmokDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC_OMOK_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

// 컨트롤 변수
public:
	CString m_strBSPlayerName;
	CString m_strWSPlayerName;

public:
	BADUKDOL m_Board[19][19];
	BOOL m_stone;
	void DrawStone(CDC* pDC, int x, int y, BADUKDOL dol);
	void CheckPointer(int _x, int _y, BADUKDOL dol);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonStart();

	// View 생성
public:
	Test * m_pTestView;
};
