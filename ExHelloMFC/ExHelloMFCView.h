﻿
// ExHelloMFCView.h: CExHelloMFCView 클래스의 인터페이스
//

#pragma once


class CExHelloMFCView : public CView
{
protected: // serialization에서만 만들어집니다.
	CExHelloMFCView() noexcept;
	DECLARE_DYNCREATE(CExHelloMFCView)

// 특성입니다.
public:
	CExHelloMFCDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CExHelloMFCView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	// 마우스의 좌표 정보
	CPoint m_cordinate;
	CString m_str;
	
	// 마우스의 좌표를 저장할 변수
	CString m_strPoint;

	
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // ExHelloMFCView.cpp의 디버그 버전
inline CExHelloMFCDoc* CExHelloMFCView::GetDocument() const
   { return reinterpret_cast<CExHelloMFCDoc*>(m_pDocument); }
#endif

