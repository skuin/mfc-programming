﻿
// MFCExClockView.cpp: CMFCExClockView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MFCExClock.h"
#endif

#include "MFCExClockDoc.h"
#include "MFCExClockView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define x1 280
#define y1 230
#define x2 400
#define y2 350
#define R 125	// 반지름
#define Center_x x1+R
#define Center_y y1+R
#define SecondHand_Length 180 // 초침의 길이
#define PI 3.141592


// CMFCExClockView

IMPLEMENT_DYNCREATE(CMFCExClockView, CView)

BEGIN_MESSAGE_MAP(CMFCExClockView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_PAINT()
END_MESSAGE_MAP()

// CMFCExClockView 생성/소멸

int temp_xs2, temp_ys2;

CMFCExClockView::CMFCExClockView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.
	m_strTimeMarker = " ";
	m_nSecond = 0;
	m_nClockPaneImage = 1;
}

CMFCExClockView::~CMFCExClockView()
{
}

BOOL CMFCExClockView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CMFCExClockView 그리기

void CMFCExClockView::OnDraw(CDC* /*pDC*/)
{
	CMFCExClockDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CMFCExClockView 인쇄

BOOL CMFCExClockView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMFCExClockView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMFCExClockView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMFCExClockView 진단

#ifdef _DEBUG
void CMFCExClockView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCExClockView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCExClockDoc* CMFCExClockView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCExClockDoc)));
	return (CMFCExClockDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCExClockView 메시지 처리기


int CMFCExClockView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetTimer(1, 100, NULL);
	return 0;
}


void CMFCExClockView::OnDestroy()
{
	CView::OnDestroy();

	KillTimer(1);
}


void CMFCExClockView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (m_nSecond == 360)
	{
		m_nSecond = 0;
	}
	++m_nSecond;
	
	Invalidate();

	CView::OnTimer(nIDEvent);
}


void CMFCExClockView::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CDC memdc;
	memdc.CreateCompatibleDC(&dc);
	
	CBitmap bitmap, *oldbitmap;
	bitmap.LoadBitmapW(IDB_BITMAP1);

	oldbitmap = memdc.SelectObject(&bitmap);
	dc.BitBlt(150, 100, 600, 600, &memdc, 0, 0, SRCCOPY);

	int i,j, x, y;
	int Second_x, Second_y;

	for (i = 1; i <= 60; i++)
	{
		/*
		x = Center_x * sin(i*PI / 30);
		y = Center_y * -cos(i*PI / 30);
		if ((i % 5) == 0) {
			j = i / 5;
			m_strTimeMarker.Format(_T("%d"), j);
			dc.TextOutW(x + R - 6, y + R - 6, m_strTimeMarker);
		}
		*/
		
		Second_x = SecondHand_Length * sin(m_nSecond * PI / 180);
		Second_y = SecondHand_Length * -cos(m_nSecond * PI / 180);
		dc.MoveTo(Center_x, Center_y);
		if (!m_bDrawMode) {
			temp_xs2 = SecondHand_Length * sin((m_nSecond - 1)*PI / 180);
			temp_ys2 = SecondHand_Length * -cos((m_nSecond - 1)*PI / 180);
		}
		else
			m_bDrawMode = FALSE;

		dc.LineTo(temp_xs2 + Center_x, temp_ys2 + Center_y);
		memdc.SelectObject(oldbitmap);
	}
}
