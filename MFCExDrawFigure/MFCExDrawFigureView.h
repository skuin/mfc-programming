﻿
// MFCExDrawFigureView.h: CMFCExDrawFigureView 클래스의 인터페이스
//

#pragma once



class CMFCExDrawFigureView : public CView
{
protected: // serialization에서만 만들어집니다.
	CMFCExDrawFigureView() noexcept;
	DECLARE_DYNCREATE(CMFCExDrawFigureView)

// 특성입니다.
public:
	CMFCExDrawFigureDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CMFCExDrawFigureView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnFreeLine();
	afx_msg void OnLine();
	afx_msg void OnEllipse();
	afx_msg void OnRectangle();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	CPoint m_ptStart;
	CPoint m_ptEnd;
	CPoint m_ptPrevPos;
	int m_nDrawType;
	bool m_flag;
	afx_msg void OnUpdateEllipse(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFreeLine(CCmdUI *pCmdUI);
	afx_msg void OnUpdateLine(CCmdUI *pCmdUI);
	afx_msg void OnUpdateRectangle(CCmdUI *pCmdUI);
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};

#ifndef _DEBUG  // MFCExDrawFigureView.cpp의 디버그 버전
inline CMFCExDrawFigureDoc* CMFCExDrawFigureView::GetDocument() const
   { return reinterpret_cast<CMFCExDrawFigureDoc*>(m_pDocument); }
#endif

