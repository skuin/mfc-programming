﻿
// MFCTestTimerView.h: CMFCTestTimerView 클래스의 인터페이스
//

#pragma once


class CMFCTestTimerView : public CView
{
protected: // serialization에서만 만들어집니다.
	CMFCTestTimerView() noexcept;
	DECLARE_DYNCREATE(CMFCTestTimerView)

// 특성입니다.
public:
	CMFCTestTimerDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CMFCTestTimerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()


public:
	bool m_bTimerRun;
	CString m_strCurrentTime;
	CString m_strCurrentDate;

	int nX, nY, nMx, nMy;
	int nX2, nY2, nMx2, nMy2;
	CRect m_Rect;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};

#ifndef _DEBUG  // MFCTestTimerView.cpp의 디버그 버전
inline CMFCTestTimerDoc* CMFCTestTimerView::GetDocument() const
   { return reinterpret_cast<CMFCTestTimerDoc*>(m_pDocument); }
#endif

