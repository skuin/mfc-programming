﻿
// MFC_ResterOpView.cpp: CMFCResterOpView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MFC_ResterOp.h"
#endif

#include "MFC_ResterOpDoc.h"
#include "MFC_ResterOpView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCResterOpView

IMPLEMENT_DYNCREATE(CMFCResterOpView, CView)

BEGIN_MESSAGE_MAP(CMFCResterOpView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

// CMFCResterOpView 생성/소멸

CMFCResterOpView::CMFCResterOpView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CMFCResterOpView::~CMFCResterOpView()
{
}

BOOL CMFCResterOpView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CMFCResterOpView 그리기

void CMFCResterOpView::OnDraw(CDC* pDC)
{
	CMFCResterOpDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CPen pen, *oldPen;
	pen.CreatePen(PS_SOLID, 1, m_PenColor);
	oldPen = pDC->SelectObject(&pen);
	pDC->Rectangle(m_ptStrat.x, m_ptStrat.y, m_ptEnd.x, m_ptEnd.y);
	pDC->SelectObject(oldPen);
	pen.DeleteObject();


	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CMFCResterOpView 인쇄

BOOL CMFCResterOpView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMFCResterOpView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMFCResterOpView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMFCResterOpView 진단

#ifdef _DEBUG
void CMFCResterOpView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCResterOpView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCResterOpDoc* CMFCResterOpView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCResterOpDoc)));
	return (CMFCResterOpDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCResterOpView 메시지 처리기


void CMFCResterOpView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nChar)
	{
	case 'R':
		m_PenColor = RGB(255, 0, 0);
		break;
	case 'G':
		m_PenColor = RGB(0, 255, 0);
		break;
	case 'B':
		m_PenColor = RGB(0, 0, 255);
		break;
	default:
		m_PenColor = RGB(0, 0, 0);

		break;
	}
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CMFCResterOpView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_ptStrat = point;
	CView::OnLButtonDown(nFlags, point);
}


void CMFCResterOpView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_ptEnd = point;
	m_flag = FALSE;
	Invalidate();
	CView::OnLButtonUp(nFlags, point);
}


void CMFCResterOpView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (nFlags == MK_LBUTTON)
	{
		CClientDC dc(this);
		dc.SetROP2(R2_NOTXORPEN);
		if (m_flag)
			dc.Rectangle(m_ptStrat.x, m_ptStrat.y, m_ptEnd.x, m_ptEnd.y);
		m_ptEnd = point;
		dc.Rectangle(m_ptStrat.x, m_ptStrat.y, m_ptEnd.x, m_ptEnd.y);
		m_flag = TRUE;
	}
	CView::OnMouseMove(nFlags, point);
}
