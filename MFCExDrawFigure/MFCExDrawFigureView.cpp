﻿
// MFCExDrawFigureView.cpp: CMFCExDrawFigureView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MFCExDrawFigure.h"
#endif

#include "MFCExDrawFigureDoc.h"
#include "MFCExDrawFigureView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define FREELINE 1
#define LINE 2
#define ELLIPSE 3
#define RECTANGLE 4

// CMFCExDrawFigureView

IMPLEMENT_DYNCREATE(CMFCExDrawFigureView, CView)

BEGIN_MESSAGE_MAP(CMFCExDrawFigureView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_COMMAND(ID_FREE_LINE, &CMFCExDrawFigureView::OnFreeLine)
	ON_COMMAND(ID_LINE, &CMFCExDrawFigureView::OnLine)
	ON_COMMAND(ID_ELLIPSE, &CMFCExDrawFigureView::OnEllipse)
	ON_COMMAND(ID_RECTANGLE, &CMFCExDrawFigureView::OnRectangle)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_UPDATE_COMMAND_UI(ID_ELLIPSE, &CMFCExDrawFigureView::OnUpdateEllipse)
	ON_UPDATE_COMMAND_UI(ID_FREE_LINE, &CMFCExDrawFigureView::OnUpdateFreeLine)
	ON_UPDATE_COMMAND_UI(ID_LINE, &CMFCExDrawFigureView::OnUpdateLine)
	ON_UPDATE_COMMAND_UI(ID_RECTANGLE, &CMFCExDrawFigureView::OnUpdateRectangle)
	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

// CMFCExDrawFigureView 생성/소멸

CMFCExDrawFigureView::CMFCExDrawFigureView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CMFCExDrawFigureView::~CMFCExDrawFigureView()
{
}

BOOL CMFCExDrawFigureView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CMFCExDrawFigureView 그리기

void CMFCExDrawFigureView::OnDraw(CDC* /*pDC*/)
{
	CMFCExDrawFigureDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CMFCExDrawFigureView 인쇄

BOOL CMFCExDrawFigureView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMFCExDrawFigureView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMFCExDrawFigureView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMFCExDrawFigureView 진단

#ifdef _DEBUG
void CMFCExDrawFigureView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCExDrawFigureView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCExDrawFigureDoc* CMFCExDrawFigureView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCExDrawFigureDoc)));
	return (CMFCExDrawFigureDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCExDrawFigureView 메시지 처리기


void CMFCExDrawFigureView::OnFreeLine()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	m_nDrawType = FREELINE;
}


void CMFCExDrawFigureView::OnLine()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	m_nDrawType = LINE;
}


void CMFCExDrawFigureView::OnEllipse()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	m_nDrawType = ELLIPSE;
}


void CMFCExDrawFigureView::OnRectangle()
{
	// TODO: 여기에 명령 처리기 코드를 추가합니다.
	m_nDrawType = RECTANGLE;
}


void CMFCExDrawFigureView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (nFlags == MK_LBUTTON) {
		CClientDC dc(this);
		dc.SetROP2(R2_NOTXORPEN);
		m_ptEnd = point;
		switch (m_nDrawType)
		{
		case FREELINE:
			dc.MoveTo(m_ptPrevPos);
			dc.LineTo(m_ptEnd);
			m_ptPrevPos = m_ptEnd;
			break;
		case LINE:
			if (m_flag) {
				dc.MoveTo(m_ptStart);
				dc.LineTo(m_ptEnd);
			}
			dc.MoveTo(m_ptStart);
			dc.LineTo(m_ptEnd);
			m_flag = TRUE;
			break;
		case ELLIPSE:
			if (m_flag)
				dc.Ellipse(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
			dc.Ellipse(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
			m_flag = TRUE;
			break;
		case RECTANGLE:
			if (m_flag)
				dc.Rectangle(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
			dc.Rectangle(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
			m_flag = TRUE;
			break;
		default:
			break;
		}
	}
	CView::OnMouseMove(nFlags, point);
}


void CMFCExDrawFigureView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CClientDC dc(this);
	m_ptEnd = point;
	switch (m_nDrawType)
	{
	case FREELINE:
		dc.MoveTo(m_ptPrevPos);
		dc.LineTo(m_ptEnd);
		break;
	case LINE:
		dc.MoveTo(m_ptStart);
		dc.LineTo(m_ptEnd);
		break;
	case ELLIPSE:
		dc.Ellipse(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
		break;
	case RECTANGLE:
		dc.Rectangle(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
		break;
	default:
		break;
	}
	m_flag = FALSE;
	CView::OnLButtonUp(nFlags, point);
}


void CMFCExDrawFigureView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_ptStart = point;
	m_ptPrevPos = point;
	CView::OnLButtonDown(nFlags, point);
}


void CMFCExDrawFigureView::OnUpdateEllipse(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	pCmdUI->SetCheck(m_nDrawType == ELLIPSE);
}


void CMFCExDrawFigureView::OnUpdateFreeLine(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	pCmdUI->SetCheck(m_nDrawType == FREELINE);

}


void CMFCExDrawFigureView::OnUpdateLine(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	pCmdUI->SetCheck(m_nDrawType == LINE);

}


void CMFCExDrawFigureView::OnUpdateRectangle(CCmdUI *pCmdUI)
{
	// TODO: 여기에 명령 업데이트 UI 처리기 코드를 추가합니다.
	pCmdUI->SetCheck(m_nDrawType == RECTANGLE);

}


void CMFCExDrawFigureView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	CMenu menu;
	menu.LoadMenuW(IDR_MAINFRAME);
	CMenu* pMenu = menu.GetSubMenu(4);
	pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, AfxGetMainWnd());
}
