﻿
// MapEditorView.h: CMapEditorView 클래스의 인터페이스
//

#pragma once
#include "CControlView.h"
//enum BADUKDOL { NONE, BLACK, WHITE };

class CMapEditorDoc;
class CMapEditorView : public CView
{
protected: // serialization에서만 만들어집니다.
	CMapEditorView() noexcept;
	DECLARE_DYNCREATE(CMapEditorView)

// 특성입니다.
public:
	CMapEditorDoc* GetDocument() const;
	CString msg;					// 메시지 처리 스트링
	CRect winrect;					// 클라이언트 영역의 크기
	CPoint winRatio;				// 윈도우의 크기가 변경되었을 때 클라이언트 영역의 크기
	int checkbit;					// 1~6 : 배경, 7~26 : 지형
	int cnt;						// 타일 아이콘을 만든 개수
	CPoint arrIcon[600];			// 타일 아이콘들의 좌표 데이터
	int IS_Move[600] = { 0, };		// 타일 아이콘들의 장애물인지 아닌지 ( 0 = 장애물아님, 1 = 장애물) 확인용
	int KindsofIcon[600] = { 0, };	// 타일 아이콘들의 종류
	bool MouseDown;					// 왼쪽 마우스가 눌러졌는지 확인 ( 0 = FALSE, 1 =TRUE)
	bool m_bStartflag = false;	// 게임스타트 상태 (false면 맵툴모드 true면 게임모드)

	// 원 움직이는 속도
	const int SPEED = 5;

	// 원 초기위치
	int x1 = 100;
	int y1 = 580;
	int x2 = 120;
	int y2 = 600;

// 작업입니다.
public:
	void DrawIcon(CDC* pDC, int x, int y);		// 아이콘을 뷰에 그리는 함수
	void DrawMap(CDC* pDC);
// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);				//윈도우가 처음 생성되었을 때 호출
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CMapEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);			//MouseMove 이벤트 처리 함수
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);			//LButtonDown 이벤트 처리 함수
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);			//LButtonUp 이벤트 처리 함수
	virtual void OnInitialUpdate();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

#ifndef _DEBUG  // MapEditorView.cpp의 디버그 버전
inline CMapEditorDoc* CMapEditorView::GetDocument() const
   { return reinterpret_cast<CMapEditorDoc*>(m_pDocument); }
#endif

