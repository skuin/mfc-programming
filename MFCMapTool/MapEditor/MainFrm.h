﻿
// MainFrm.h: CMainFrame 클래스의 인터페이스
//

#pragma once
#include "MapEditorView.h"
#include "CControlView.h"

class CMainFrame : public CFrameWnd
{
	
protected: // serialization에서만 만들어집니다.
	CMainFrame() noexcept;
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:
// 작업입니다.
public:
	CSplitterWnd m_MainSplitter;	//메인 Document
	CSplitterWnd m_SubSplitter;		//서브 Document

	CMapEditorView* m_pMainView;	//Main View	-> 맵을 그리는 창
	CControlView* m_pControlView;	//Control View	-> 타일을 선택하는 창
// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);	//윈도우 생성 시 호출되는 창

// 구현입니다.
public:
	CRect MainClientRect;							//Main View의 크기를 포함(x1, y1, x2, y2)
	CRect FormViewClientRect;						//Control View의 크기를 포함(x1, y1, x2, y2)

	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	CToolBar          m_wndToolBar;
	CStatusBar        m_wndStatusBar;

// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
};


