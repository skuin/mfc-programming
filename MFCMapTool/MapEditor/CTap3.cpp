﻿// CTap3.cpp: 구현 파일
//

#include "stdafx.h"
#include "MapEditor.h"
#include "CTap3.h"
#include "afxdialogex.h"


// CTap3 대화 상자

IMPLEMENT_DYNAMIC(CTap3, CDialogEx)

CTap3::CTap3(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TAP3, pParent)
{

}

CTap3::~CTap3()
{
}

void CTap3::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTap3, CDialogEx)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CTap3 메시지 처리기


void CTap3::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
}
