﻿
// ExDrawPictureView.cpp: CExDrawPictureView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "ExDrawPicture.h"
#endif

#include "ExDrawPictureDoc.h"
#include "ExDrawPictureView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExDrawPictureView

IMPLEMENT_DYNCREATE(CExDrawPictureView, CView)

BEGIN_MESSAGE_MAP(CExDrawPictureView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

// CExDrawPictureView 생성/소멸

CExDrawPictureView::CExDrawPictureView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CExDrawPictureView::~CExDrawPictureView()
{
}

BOOL CExDrawPictureView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CExDrawPictureView 그리기

void CExDrawPictureView::OnDraw(CDC* pDC)
{
	CExDrawPictureDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	//pDC->Rectangle(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);

	//pDC->Ellipse(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);

	//pDC->MoveTo(m_ptStart);
	//pDC->LineTo(m_ptEnd);

	pDC->Polygon(m_ptData,5);

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CExDrawPictureView 인쇄

BOOL CExDrawPictureView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CExDrawPictureView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CExDrawPictureView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CExDrawPictureView 진단

#ifdef _DEBUG
void CExDrawPictureView::AssertValid() const
{
	CView::AssertValid();
}

void CExDrawPictureView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CExDrawPictureDoc* CExDrawPictureView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CExDrawPictureDoc)));
	return (CExDrawPictureDoc*)m_pDocument;
}
#endif //_DEBUG


// CExDrawPictureView 메시지 처리기


void CExDrawPictureView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_ptStart = point;
	CView::OnLButtonDown(nFlags, point);
}


void CExDrawPictureView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_ptEnd = point;

	m_ptData[0].x = m_ptStart.x + (m_ptEnd.x - m_ptStart.x)/ 2;
	m_ptData[0].y = m_ptStart.y;
	m_ptData[1].x = m_ptStart.x;
	m_ptData[1].y = m_ptStart.y + (m_ptEnd.y - m_ptStart.y) / 2;
	m_ptData[2].x = m_ptStart.x + (m_ptEnd.x - m_ptStart.x) / 4;
	m_ptData[2].y = m_ptEnd.y;
	m_ptData[3].x = m_ptStart.x + 3 * (m_ptEnd.x - m_ptStart.x) / 4;
	m_ptData[3].y = m_ptEnd.y;
	m_ptData[4].x = m_ptEnd.x;
	m_ptData[4].y = m_ptStart.y + (m_ptEnd.y - m_ptStart.y) / 2;
	Invalidate();

	CView::OnLButtonUp(nFlags, point);
}


void CExDrawPictureView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (nFlags & MK_LBUTTON)
	{
		m_ptEnd = point;

		m_ptData[0].x = m_ptStart.x + (m_ptEnd.x - m_ptStart.x) / 2;
		m_ptData[0].y = m_ptStart.y;
		m_ptData[1].x = m_ptStart.x;
		m_ptData[1].y = m_ptStart.y + (m_ptEnd.y - m_ptStart.y) / 2;
		m_ptData[2].x = m_ptStart.x + (m_ptEnd.x - m_ptStart.x) / 4;
		m_ptData[2].y = m_ptEnd.y;
		m_ptData[3].x = m_ptStart.x + 3 * (m_ptEnd.x - m_ptStart.x) / 4;
		m_ptData[3].y = m_ptEnd.y;
		m_ptData[4].x = m_ptEnd.x;
		m_ptData[4].y = m_ptStart.y + (m_ptEnd.y - m_ptStart.y) / 2;
		Invalidate();
	}
	CView::OnMouseMove(nFlags, point);
}
