﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MapEditor.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_FORMVIEW                    101
#define IDR_MAINFRAME                   128
#define IDR_MapEditorTYPE               130
#define IDD_TAP1                        313
#define IDD_TAP2                        314
#define IDD_TAP3                        320
#define IDB_BITMAP_TILE1                322
#define IDB_BITMAP_TILE2                329
#define IDB_BITMAP2                     330
#define IDB_BITMAP_TILE3                330
#define IDB_BITMAP_TILE4                332
#define IDB_BITMAP_TILE5                333
#define IDB_BITMAP_TILE6                334
#define IDB_RAND2                       336
#define IDB_RAND3                       337
#define IDB_RAND4                       338
#define IDB_RAND5                       339
#define IDB_RAND6                       340
#define IDB_RAND7                       341
#define IDB_RAND8                       342
#define IDB_RAND9                       343
#define IDB_RAND10                      344
#define IDB_RAND11                      345
#define IDB_RAND12                      346
#define IDB_RAND13                      347
#define IDB_RAND14                      348
#define IDB_RAND15                      349
#define IDB_RAND16                      350
#define IDB_RAND17                      351
#define IDB_RAND18                      352
#define IDB_RAND19                      353
#define IDB_RAND20                      354
#define IDB_RAND1                       355
#define IDC_TAB1                        1003
#define IDC_BUTTON1                     1008
#define IDC_BUTTON2                     1009
#define IDC_TAP1_CHOICE                 1010
#define IDC_RADIO1                      1011
#define IDC_RADIO2                      1012
#define IDC_RADIO3                      1013
#define IDC_RADIO4                      1014
#define IDC_RADIO5                      1015
#define IDC_RADIO6                      1016
#define IDC_TILE1                       1018
#define IDC_TILE2                       1019
#define IDC_TILE3                       1020
#define IDC_TILE4                       1021
#define IDC_TILE5                       1022
#define IDC_TILE6                       1023
#define IDC_CHECK1                      1024
#define IDC_CHECK2                      1025
#define IDC_RADIO7                      1025
#define IDC_CHECK3                      1026
#define IDC_RADIO8                      1026
#define IDC_CHECK4                      1027
#define IDC_RADIO9                      1027
#define IDC_CHECK5                      1028
#define IDC_RADIO10                     1028
#define IDC_CHECK6                      1029
#define IDC_RADIO11                     1029
#define IDC_RADIO12                     1030
#define IDC_RADIO13                     1031
#define IDC_RADIO14                     1032
#define IDC_RADIO15                     1033
#define IDC_RADIO16                     1034
#define IDC_RADIO17                     1035
#define IDC_RADIO18                     1036
#define IDC_RADIO19                     1037
#define IDC_RADIO20                     1038
#define IDC_RAND3                       1041
#define IDC_RAND4                       1042
#define IDC_RAND5                       1043
#define IDC_RAND6                       1044
#define IDC_RAND7                       1045
#define IDC_RAND8                       1046
#define IDC_RAND9                       1047
#define IDC_RAND10                      1048
#define IDC_RAND11                      1049
#define IDC_RAND12                      1050
#define IDC_RAND13                      1051
#define IDC_RAND14                      1052
#define IDC_RAND15                      1053
#define IDC_RAND16                      1054
#define IDC_RAND17                      1055
#define IDC_RAND18                      1056
#define IDC_RAND19                      1057
#define IDC_RAND20                      1058
#define IDC_RAND1                       1059
#define IDC_RAND2                       1060

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        358
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1061
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
