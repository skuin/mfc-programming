﻿
// MFC_OmokDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "MFC_Omok.h"
#include "MFC_OmokDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME 
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCOmokDlg 대화 상자



CMFCOmokDlg::CMFCOmokDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC_OMOK_DIALOG, pParent)
	, m_strBSPlayerName(_T(""))
	, m_strWSPlayerName(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCOmokDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_EDIT_BLACK_STONE, (CWnd &)m_strBSPlayerName);
	//DDX_Control(pDX, IDC_EDIT_WHITE_STONE, (CWnd &)m_strWSPlayerName);
}

BEGIN_MESSAGE_MAP(CMFCOmokDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_START, &CMFCOmokDlg::OnBnClickedButtonStart)
END_MESSAGE_MAP()


// CMFCOmokDlg 메시지 처리기

BOOL CMFCOmokDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	//m_pTestView = new Test;
	//m_pTestView->Create(NULL, L"", WS_CHILD)


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMFCOmokDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMFCOmokDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this);
		dc.Rectangle(10, 10, 590, 590);
		int x, y;
		for (y = 0; y < 19; y++)
		{
			dc.MoveTo(30, 30 + y * 30);
			dc.LineTo(570, 30 + y * 30);
		}

		for (x = 0; x < 19; x++)
		{
			dc.MoveTo(30 + x * 30, 30);
			dc.LineTo(30 + x * 30, 570);
		}
		if (m_Board[x][y] != NONE)
		{
			for (x = 0; x < 19; x++)
				for (y = 0; y < 19; y++)
				{
					DrawStone(&dc, x, y, m_Board[x][y]);
				}
		}

	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMFCOmokDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMFCOmokDlg::DrawStone(CDC * pDC, int x, int y, BADUKDOL dol)
{
	if (dol == BLACK)
		pDC->SelectStockObject(BLACK_BRUSH);
	else if (dol == WHITE)
		pDC->SelectStockObject(WHITE_BRUSH);
	if (dol != NONE)
		pDC->Ellipse(x * 30, y * 30, x * 30+30 , y * 30+30);
}

void CMFCOmokDlg::CheckPointer(int _x, int _y, BADUKDOL dol)
{
	int x, y;
	int count;

	//가로
	x = _x;
	y = _y;
	count = 0;
	// x값만 감소시켜 stone과 다를때까지 감소
	while (m_Board[x - 1][_y] == dol && x > 0)
		x--;
	// x값만 증가시켜 stone과 다를때까지 증가
	while (m_Board[x++][_y] == dol && x <= 18)
		count++;
	if (count == 5)//x 값을 증가 시켜서 5가 되는 경우 GameWin 메시지 출력
	{
		if (dol == BLACK)
			MessageBox(m_strBSPlayerName + "승리!", MB_OK);
		else if (dol == WHITE)
			MessageBox(m_strWSPlayerName + "승리!", MB_OK);
	}

	//세로
	x = _x;
	y = _y;
	count = 0;
	// y값을 변화시켜 오목을 체크한다.
	while (m_Board[_x][y - 1] == dol && y > 0)
		y--;
	while (m_Board[_x][y++] == dol && y <= 18)
		count++;
	if (count == 5)
	{
		if (dol == BLACK)
			MessageBox(m_strBSPlayerName + "승리!", MB_OK);
		else if (dol == WHITE)
			MessageBox(m_strWSPlayerName + "승리!", MB_OK);
	}

	//대각선↘
	x = _x;
	y = _y;
	count = 0;
	// x, y 모두 변화시켜 오목을 체크.
	while (m_Board[x - 1][y - 1] == dol && x > 0 && y > 0)
	{
		y--;
		x--;
	}
	while (m_Board[x++][y++] == dol && x <= 18 && y <= 18)
		count++;
	if (count == 5)
	{
		if (dol == BLACK)
			MessageBox(m_strBSPlayerName + "승리!", MB_OK);
		else if (dol == WHITE)
			MessageBox(m_strWSPlayerName + "승리!", MB_OK);
	}

	//대각선↙
	x = _x;
	y = _y;
	count = 0;
	// x, y 모두 변화시켜 오목을 체크.
	while (m_Board[x + 1][y - 1] == dol && x < 18 && y > 0)
	{
		x++;
		y--;
	}
	while (m_Board[x--][y++] == dol && x >= 0 && y <= 18)
		count++;
	if (count == 5)
	{
		if (dol == BLACK)
			MessageBox(m_strBSPlayerName + "승리!", MB_OK);
		else if (dol == WHITE)
			MessageBox(m_strWSPlayerName + "승리!", MB_OK);
	}
}



void CMFCOmokDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	int ax, ay;
	int rex, rey;

	ax = point.x / 30;
	rex = point.x % 30;
	/*
	if ((0 < rex)&(rex < 30))
		return;
	else if (rex < 10)
		ax = ax - 1;
		*/
	ay = point.y / 30;
	rey = point.y % 30;
	
	/*
	if ((rey > 10)&(rey < 20))
		return;
	else if (rey < 10)
		ay = ay - 1;
	*/

	if ((ax < 0) || (ax >= 19) || (ay < 0) || (ay >= 19))
		return;
	if (m_Board[ax][ay] != NONE)
		return;
	m_Board[ax][ay] = (m_stone ? WHITE : BLACK);
	CClientDC dc(this);
	// 바둑돌을 그린다.
	DrawStone(&dc, ax, ay, m_Board[ax][ay]);
	// 승패검사를 한다.
	CheckPointer(ax, ay, m_Board[ax][ay]);
	// 바둑돌의 색을 바꾼다.
	m_stone = !m_stone;

	CDialogEx::OnLButtonDown(nFlags, point);
}


void CMFCOmokDlg::OnBnClickedButtonStart()
{
	UpdateData(TRUE);

	GetDlgItemText(IDC_EDIT_WHITE_STONE, m_strWSPlayerName);
	GetDlgItemText(IDC_EDIT_BLACK_STONE, m_strBSPlayerName);

	if (m_strBSPlayerName.IsEmpty() == TRUE || m_strWSPlayerName.IsEmpty() == TRUE)
	{
		MessageBox(_T("게임 참여자 이름을 입력 하십시오"), MB_OK);
	}
	else
	{
		GetDlgItem(IDC_EDIT_WHITE_STONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_BLACK_STONE)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);
		
		int x, y;
		for (x = 0; x < 19; x++)
			for (y = 0; y < 19; y++)
				m_Board[x][y] = NONE;
		Invalidate(TRUE);

	}

}
