﻿#pragma once


// CTap3 대화 상자

class CTap3 : public CDialogEx
{
	DECLARE_DYNAMIC(CTap3)

public:
	CTap3(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTap3();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TAP3 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};
