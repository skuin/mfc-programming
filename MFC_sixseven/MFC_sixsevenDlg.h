﻿
// MFC_sixsevenDlg.h: 헤더 파일
//

#pragma once


// CMFCsixsevenDlg 대화 상자
class CMFCsixsevenDlg : public CDialogEx
{
// 생성입니다.
public:
	CMFCsixsevenDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC_SIXSEVEN_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	//  Edit Controll 입력 숫자 저장
	int m_inputNum;
	// Edit Controll 실행 결과 저장 
	CString m_strResult;

	// 실행 버튼 클릭
	afx_msg void OnBnClickedStart();

	// 입력 받은 int형을 CString으로 변환 시킨후 저장할 변수
	CString m_strNum;

	// 각각 6의 배수 7의 배수, 6의 7의 공배수를 저장할 변수
	int m_iSum1, m_iSum2, m_iSum3;
	// int 형의 공배수를 CString형으로 변환 시킨후 저장할 변수
	CString m_strSum;
};
