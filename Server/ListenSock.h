﻿#pragma once

// CListenSock 명령 대상

class CListenSock : public CAsyncSocket
{
public:
	CListenSock();
	virtual ~CListenSock();
	virtual void OnAccept(int nErrorCode);
};


