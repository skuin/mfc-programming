﻿
// MFCExClockView.h: CMFCExClockView 클래스의 인터페이스
//

#pragma once


class CMFCExClockView : public CView
{
protected: // serialization에서만 만들어집니다.
	CMFCExClockView() noexcept;
	DECLARE_DYNCREATE(CMFCExClockView)

// 특성입니다.
public:
	CMFCExClockDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CMFCExClockView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()


public:
	CString m_strTimeMarker;
	int m_nSecond;
	int m_nClockPaneImage;
	bool m_bDrawMode;

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
};

#ifndef _DEBUG  // MFCExClockView.cpp의 디버그 버전
inline CMFCExClockDoc* CMFCExClockView::GetDocument() const
   { return reinterpret_cast<CMFCExClockDoc*>(m_pDocument); }
#endif

