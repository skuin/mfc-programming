﻿// CControlView.cpp: 구현 파일
//

#include "stdafx.h"
#include "MapEditor.h"
#include "CControlView.h"
#include "MainFrm.h"
#include "CTap1.h"
#include "CTap2.h"
#include "CTap3.h"

// CControlView

IMPLEMENT_DYNCREATE(CControlView, CFormView)

CControlView::CControlView(CWnd* pParent /*=nullptr*/)
	: CFormView(IDD_FORMVIEW)
{
}

CControlView::~CControlView()
{
}

BEGIN_MESSAGE_MAP(CControlView, CView)
	ON_WM_CREATE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CControlView::OnTcnSelchangeTab1)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CControlView 그리기

void CControlView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: 여기에 그리기 코드를 추가합니다.
}


// CControlView 진단

#ifdef _DEBUG
void CControlView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CControlView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG

void CControlView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_Tap);
}

// CControlView 메시지 처리기


int CControlView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	return 0;
}

void CControlView::OnInitialUpdate()		//뷰가 최초에 윈도우에 나타날 때 호출
{
	CFormView::OnInitialUpdate();

	m_Tap.InsertItem(0, _T("타일"));						//첫 번째 탭의 이름
	m_Tap.InsertItem(1, _T("지형"));						//두 번째 탭의 이름

	m_Tap.SetCurSel(0);										//첫 번째 탭을 지정
	CRect rect;
	m_Tap.GetWindowRect(&rect);								//탭의 사이즈를 rect에 저장

	pDlg1 = new CTap1;										//초기화
	pDlg1->Create(IDD_TAP1, this);							//탭 컨트롤 1을 지정
	pDlg1->MoveWindow(0, 30, rect.Width(), rect.Height());	//0, 30 위치에 탭1을 rect의 가로 세로 길이 만큼 이동
	pDlg1->ShowWindow(SW_SHOW);								//탭1 을 출력
	
	pDlg2 = new CTap2;										//초기화
	pDlg2->Create(IDD_TAP2, this);							//탭 컨트롤 2를 지정
	pDlg2->MoveWindow(0, 30, rect.Width(), rect.Height());	//0, 30 위치에 탭2를 rect의 가로 세로 길이 만큼 이동
	pDlg2->ShowWindow(SW_HIDE);								//탭2를 출력

}


BOOL CControlView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	return CFormView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}


void CControlView::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int sel = m_Tap.GetCurSel();			//선택한 해당 탭의 번호를 sel에 저장

	switch (sel)
	{
	case 0:								//탭1이 선택될 때
		pDlg1->ShowWindow(SW_SHOW);		//탭1을 화면에 뿌림
		pDlg2->ShowWindow(SW_HIDE);		//탭2를 화면에서 사라지게함
		break;
	case 1:								//탭2가 선택될 때
		pDlg1->ShowWindow(SW_HIDE);		//탭1을 화면에서 사라지게함
		pDlg2->ShowWindow(SW_SHOW);		//탭2를 화면에 뿌림
		break;
	default:
		break;
	}
	*pResult = 0;
}

