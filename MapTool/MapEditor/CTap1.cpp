﻿// CTap1.cpp: 구현 파일
//

#include "stdafx.h"
#include "MapEditor.h"
#include "MapEditorView.h"
#include "CTap1.h"
#include "afxdialogex.h"
#include "MainFrm.h"


// CTap1 대화 상자

IMPLEMENT_DYNAMIC(CTap1, CDialogEx)

CTap1::CTap1(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TAP1, pParent)
{

}

CTap1::~CTap1()
{
}

void CTap1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	// 컨트롤 변수와 컨트롤을 연결
	DDX_Control(pDX, IDC_TILE1, m_Tile1);
	DDX_Control(pDX, IDC_TILE2, m_Tile2);
	DDX_Control(pDX, IDC_TILE3, m_Tile3);
	DDX_Control(pDX, IDC_TILE4, m_Tile4);
	DDX_Control(pDX, IDC_TILE5, m_Tile5);
	DDX_Control(pDX, IDC_TILE6, m_Tile6);
}


BEGIN_MESSAGE_MAP(CTap1, CDialogEx)
	ON_WM_PAINT()
	// 1~6 체크박스의 이벤트 처리함수를 매시지맵에 생성
	ON_BN_CLICKED(IDC_CHECK1, &CTap1::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CTap1::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, &CTap1::OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK4, &CTap1::OnBnClickedCheck4)
	ON_BN_CLICKED(IDC_CHECK5, &CTap1::OnBnClickedCheck5)
	ON_BN_CLICKED(IDC_CHECK6, &CTap1::OnBnClickedCheck6)
END_MESSAGE_MAP()


// CTap1 메시지 처리기


void CTap1::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.
	
	// 비트맵 변수에 해당 비트맵을 연결
	tile1 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_TILE1));
	tile2 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_TILE2));
	tile3 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_TILE3));
	tile4 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_TILE4));
	tile5 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_TILE5));
	tile6 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_TILE6));

	// 픽쳐 컨트롤 변수에 비트맵을 각각 연결 시켜줌 (해당 컨트롤에 비트맵이 그려짐)
	m_Tile1.SetBitmap(tile1);
	m_Tile2.SetBitmap(tile2);
	m_Tile3.SetBitmap(tile3);
	m_Tile4.SetBitmap(tile4);
	m_Tile5.SetBitmap(tile5);
	m_Tile6.SetBitmap(tile6);
}


//////////////////////// 1~6의 체크박스에 해당하는 이벤트 처리함수 //////////////////////////
void CTap1::OnBnClickedCheck1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();			//메인 프레임의 윈도우을 가져옴
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);		//메인 프레임의 0행 1열(CControlView의 위치)위치의 뷰의 주소를 pView에 초기화
	
	pView->checkbit = 1;			// MainView(MapEditorView)의 checkbit 값을 변경
}


void CTap1::OnBnClickedCheck2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 2;
}


void CTap1::OnBnClickedCheck3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 3;
}


void CTap1::OnBnClickedCheck4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 4;
}


void CTap1::OnBnClickedCheck5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 5;
}


void CTap1::OnBnClickedCheck6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 6;
}
////////////////////////////////////////////////////////////////////////////////////////