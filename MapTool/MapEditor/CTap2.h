﻿#pragma once


// CTap2 대화 상자

class CTap2 : public CDialogEx
{
	DECLARE_DYNAMIC(CTap2)

public:
	CTap2(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTap2();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TAP2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	// 지형을 만들기 위함 21개의 비트맵 변수
	HBITMAP rand1, rand2, rand3, rand4, rand5, rand6, rand7, rand8, rand9, rand10, rand11, rand12, rand13, rand14, rand15, rand16, rand17, rand18, rand19, rand20;
	// 해당 픽쳐 컨트롤의 컨트롤 변수
	CStatic m_Rand1;
	CStatic m_Rand2;
	CStatic m_Rand3;
	CStatic m_Rand4;
	CStatic m_Rand5;
	CStatic m_Rand6;
	CStatic m_Rand7;
	CStatic m_Rand8;
	CStatic m_Rand9;
	CStatic m_Rand10;
	CStatic m_Rand11;
	CStatic m_Rand12;
	CStatic m_Rand13;
	CStatic m_Rand14;
	CStatic m_Rand15;
	CStatic m_Rand16;
	CStatic m_Rand17;
	CStatic m_Rand18;
	CStatic m_Rand19;
	CStatic m_Rand20;

	//21개의 라디오 버튼의 이벤트 처리 함수
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio3();
	afx_msg void OnBnClickedRadio4();
	afx_msg void OnBnClickedRadio5();
	afx_msg void OnBnClickedRadio6();
	afx_msg void OnBnClickedRadio7();
	afx_msg void OnBnClickedRadio8();
	afx_msg void OnBnClickedRadio9();
	afx_msg void OnBnClickedRadio10();
	afx_msg void OnBnClickedRadio11();
	afx_msg void OnBnClickedRadio12();
	afx_msg void OnBnClickedRadio13();
	afx_msg void OnBnClickedRadio14();
	afx_msg void OnBnClickedRadio15();
	afx_msg void OnBnClickedRadio16();
	afx_msg void OnBnClickedRadio17();
	afx_msg void OnBnClickedRadio18();
	afx_msg void OnBnClickedRadio19();
	afx_msg void OnBnClickedRadio20();
};
