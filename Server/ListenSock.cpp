﻿// ListenSock.cpp: 구현 파일
//

#include "stdafx.h"
#include "Server.h"
#include "ListenSock.h"


// CListenSock

CListenSock::CListenSock()
{
}

CListenSock::~CListenSock()
{
}


// CListenSock 멤버 함수


void CListenSock::OnAccept(int nErrorCode)
{
	((CServerApp*)AfxGetApp())->Accept(); // 접속허용을 위해 호출
	CAsyncSocket::OnAccept(nErrorCode);
}
