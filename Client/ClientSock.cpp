﻿// ClientSock.cpp: 구현 파일
//

#include "stdafx.h"
#include "Client.h"
#include "ClientSock.h"


// CClientSock

CClientSock::CClientSock()
{
}

CClientSock::~CClientSock()
{
}


// CClientSock 멤버 함수


void CClientSock::OnReceive(int nErrorCode)
{
	((CClientApp*)AfxGetApp())->ReceiveData();
	CAsyncSocket::OnReceive(nErrorCode);
}


void CClientSock::OnClose(int nErrorCode)
{
	((CClientApp*)AfxGetApp())->CloseChild();
	CAsyncSocket::OnClose(nErrorCode);
}
