﻿#pragma once
#include <atlimage.h>

// CTap1 대화 상자

class CTap1 : public CDialogEx
{
	DECLARE_DYNAMIC(CTap1)

public:
	CTap1(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTap1();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TAP1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	HBITMAP tile1, tile2, tile3, tile4, tile5, tile6;	// 타일로 사용할 비트맵 6개

	// 1~6 픽쳐 컨트롤에 해당하는 컨트롤 변수
	CStatic m_Tile1;
	CStatic m_Tile2;
	CStatic m_Tile3;
	CStatic m_Tile4;
	CStatic m_Tile5;
	CStatic m_Tile6;

	// 1~6 체크박스에 해당하는 컨트롤 변수
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	afx_msg void OnBnClickedCheck4();
	afx_msg void OnBnClickedCheck5();
	afx_msg void OnBnClickedCheck6();
};
