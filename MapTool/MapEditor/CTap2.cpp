﻿// CTap2.cpp: 구현 파일
//

#include "stdafx.h"
#include "MapEditor.h"
#include "CTap2.h"
#include "afxdialogex.h"
#include "MainFrm.h"


// CTap2 대화 상자

IMPLEMENT_DYNAMIC(CTap2, CDialogEx)

CTap2::CTap2(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TAP2, pParent)
{

}

CTap2::~CTap2()
{
}

void CTap2::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	// 컨트롤 변수와 컨트롤을 연결
	DDX_Control(pDX, IDC_RAND3, m_Rand3);
	DDX_Control(pDX, IDC_RAND4, m_Rand4);
	DDX_Control(pDX, IDC_RAND5, m_Rand5);
	DDX_Control(pDX, IDC_RAND6, m_Rand6);
	DDX_Control(pDX, IDC_RAND7, m_Rand7);
	DDX_Control(pDX, IDC_RAND8, m_Rand8);
	DDX_Control(pDX, IDC_RAND9, m_Rand9);
	DDX_Control(pDX, IDC_RAND10, m_Rand10);
	DDX_Control(pDX, IDC_RAND11, m_Rand11);
	DDX_Control(pDX, IDC_RAND12, m_Rand12);
	DDX_Control(pDX, IDC_RAND13, m_Rand13);
	DDX_Control(pDX, IDC_RAND14, m_Rand14);
	DDX_Control(pDX, IDC_RAND15, m_Rand15);
	DDX_Control(pDX, IDC_RAND16, m_Rand16);
	DDX_Control(pDX, IDC_RAND17, m_Rand17);
	DDX_Control(pDX, IDC_RAND18, m_Rand18);
	DDX_Control(pDX, IDC_RAND19, m_Rand19);
	DDX_Control(pDX, IDC_RAND20, m_Rand20);
	DDX_Control(pDX, IDC_RAND1, m_Rand1);
	DDX_Control(pDX, IDC_RAND2, m_Rand2);
}


BEGIN_MESSAGE_MAP(CTap2, CDialogEx)
	ON_WM_PAINT()
	// 1~6 라디오 버튼의 이벤트 처리함수를 매시지맵에 생성
	ON_BN_CLICKED(IDC_RADIO1, &CTap2::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CTap2::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, &CTap2::OnBnClickedRadio3)
	ON_BN_CLICKED(IDC_RADIO4, &CTap2::OnBnClickedRadio4)
	ON_BN_CLICKED(IDC_RADIO5, &CTap2::OnBnClickedRadio5)
	ON_BN_CLICKED(IDC_RADIO6, &CTap2::OnBnClickedRadio6)
	ON_BN_CLICKED(IDC_RADIO7, &CTap2::OnBnClickedRadio7)
	ON_BN_CLICKED(IDC_RADIO8, &CTap2::OnBnClickedRadio8)
	ON_BN_CLICKED(IDC_RADIO9, &CTap2::OnBnClickedRadio9)
	ON_BN_CLICKED(IDC_RADIO10, &CTap2::OnBnClickedRadio10)
	ON_BN_CLICKED(IDC_RADIO11, &CTap2::OnBnClickedRadio11)
	ON_BN_CLICKED(IDC_RADIO12, &CTap2::OnBnClickedRadio12)
	ON_BN_CLICKED(IDC_RADIO13, &CTap2::OnBnClickedRadio13)
	ON_BN_CLICKED(IDC_RADIO14, &CTap2::OnBnClickedRadio14)
	ON_BN_CLICKED(IDC_RADIO15, &CTap2::OnBnClickedRadio15)
	ON_BN_CLICKED(IDC_RADIO16, &CTap2::OnBnClickedRadio16)
	ON_BN_CLICKED(IDC_RADIO17, &CTap2::OnBnClickedRadio17)
	ON_BN_CLICKED(IDC_RADIO18, &CTap2::OnBnClickedRadio18)
	ON_BN_CLICKED(IDC_RADIO19, &CTap2::OnBnClickedRadio19)
	ON_BN_CLICKED(IDC_RADIO20, &CTap2::OnBnClickedRadio20)
END_MESSAGE_MAP()


// CTap2 메시지 처리기


void CTap2::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CDialogEx::OnPaint()을(를) 호출하지 마십시오.

	// 비트맵 변수에 해당 비트맵을 연결
	rand1 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND1));
	rand2 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND2));
	rand3 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND3));
	rand4 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND4));
	rand5 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND5));
	rand6 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND6));
	rand7 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND7));
	rand8 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND8));
	rand9 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND9));
	rand10 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND10));
	rand11 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND11));
	rand12 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND12));
	rand13 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND13));
	rand14 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND14));
	rand15 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND15));
	rand16 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND16));
	rand17 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND17));
	rand18 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND18));
	rand19 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND19));
	rand20 = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_RAND20));

	// 픽쳐 컨트롤 변수에 비트맵을 각각 연결 시켜줌 (해당 컨트롤에 비트맵이 그려짐)
	m_Rand1.SetBitmap(rand1);
	m_Rand2.SetBitmap(rand2);
	m_Rand3.SetBitmap(rand3);
	m_Rand4.SetBitmap(rand4);
	m_Rand5.SetBitmap(rand5);
	m_Rand6.SetBitmap(rand6);
	m_Rand7.SetBitmap(rand7);
	m_Rand8.SetBitmap(rand8);
	m_Rand9.SetBitmap(rand9);
	m_Rand10.SetBitmap(rand10);
	m_Rand11.SetBitmap(rand11);
	m_Rand12.SetBitmap(rand12);
	m_Rand13.SetBitmap(rand13);
	m_Rand14.SetBitmap(rand14);
	m_Rand15.SetBitmap(rand15);
	m_Rand16.SetBitmap(rand16);
	m_Rand17.SetBitmap(rand17);
	m_Rand18.SetBitmap(rand18);
	m_Rand19.SetBitmap(rand19);
	m_Rand20.SetBitmap(rand20);
}

//////////////////////// 1~20의 라디오 컨트롤에 해당하는 이벤트 처리함수 //////////////////////////
void CTap2::OnBnClickedRadio1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();										//메인 프레임의 윈도우을 가져옴
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);			//메인 프레임의 0행 1열(CControlView의 위치)위치의 뷰의 주소를 pView에 초기화

	pView->checkbit = 7;			// MainView(MapEditorView)의 checkbit 값을 변경
}


void CTap2::OnBnClickedRadio2()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 8;
}


void CTap2::OnBnClickedRadio3()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 9;
}


void CTap2::OnBnClickedRadio4()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 10;
}


void CTap2::OnBnClickedRadio5()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 11;
}


void CTap2::OnBnClickedRadio6()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 12;
}


void CTap2::OnBnClickedRadio7()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 13;
}


void CTap2::OnBnClickedRadio8()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 14;
}


void CTap2::OnBnClickedRadio9()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 15;
}


void CTap2::OnBnClickedRadio10()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 16;
}


void CTap2::OnBnClickedRadio11()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 17;
}


void CTap2::OnBnClickedRadio12()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 18;
}


void CTap2::OnBnClickedRadio13()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 19;
}


void CTap2::OnBnClickedRadio14()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 20;
}


void CTap2::OnBnClickedRadio15()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 21;
}


void CTap2::OnBnClickedRadio16()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 22;
}


void CTap2::OnBnClickedRadio17()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 23;
}


void CTap2::OnBnClickedRadio18()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 24;
}


void CTap2::OnBnClickedRadio19()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 25;
}


void CTap2::OnBnClickedRadio20()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMapEditorView* pView = (CMapEditorView*)pFrame->m_MainSplitter.GetPane(0, 1);

	pView->checkbit = 26;
}
////////////////////////////////////////////////////////////////////////////////////////////////////