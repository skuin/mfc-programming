﻿// ChildSock.cpp: 구현 파일
//

#include "stdafx.h"
#include "Server.h"
#include "ChildSock.h"


// CChildSock

CChildSock::CChildSock()
{
}

CChildSock::~CChildSock()
{
}


// CChildSock 멤버 함수


void CChildSock::OnClose(int nErrorCode)
{
	((CServerApp*)AfxGetApp())->CloseChild(); // 소켓을 닫는다. 
	CAsyncSocket::OnClose(nErrorCode);
}


void CChildSock::OnReceive(int nErrorCode)
{
	((CServerApp*)AfxGetApp())->ReceiveData(); // 데이터를 읽어 온다.
	CAsyncSocket::OnReceive(nErrorCode);
}
