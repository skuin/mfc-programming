﻿
// MFCTestTimerView.cpp: CMFCTestTimerView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MFCTestTimer.h"
#endif

#include "MFCTestTimerDoc.h"
#include "MFCTestTimerView.h"

#define STEP 10
#define R 50

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCTestTimerView

IMPLEMENT_DYNCREATE(CMFCTestTimerView, CView)

BEGIN_MESSAGE_MAP(CMFCTestTimerView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
//	ON_WM_RBUTTONDOWN()
	ON_WM_TIMER()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CMFCTestTimerView 생성/소멸

CMFCTestTimerView::CMFCTestTimerView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.
	nX = nY = R;
	nX2 = nY2 = R;
	nMx = nMy = STEP;
	nMx2 = nMy2 = 30;


}

CMFCTestTimerView::~CMFCTestTimerView()
{
}

BOOL CMFCTestTimerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CMFCTestTimerView 그리기

void CMFCTestTimerView::OnDraw(CDC* pDC)
{
	CMFCTestTimerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
	CBrush brush,brush2, *oldBrush;
	brush.CreateSolidBrush(RGB(255, 0, 0));
	oldBrush = pDC->SelectObject(&brush);
	pDC->Ellipse(200 + nX2 - 20, nY2 - 20, 200 + nX2 + 20, nY2 + 20);
	brush2.CreateSolidBrush(RGB(0, 255, 0));
	oldBrush = pDC->SelectObject(&brush2);
	pDC->Ellipse(nX - R, nY - R, nX + R, nY + R);

}


// CMFCTestTimerView 인쇄

BOOL CMFCTestTimerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMFCTestTimerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMFCTestTimerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMFCTestTimerView 진단

#ifdef _DEBUG
void CMFCTestTimerView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCTestTimerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCTestTimerDoc* CMFCTestTimerView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCTestTimerDoc)));
	return (CMFCTestTimerDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCTestTimerView 메시지 처리기


int CMFCTestTimerView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	//SetTimer(1, 1000, NULL);
	//m_bTimerRun = TRUE;
	return 0;
}


void CMFCTestTimerView::OnDestroy()
{
	CView::OnDestroy();
	
	KillTimer(1);
	KillTimer(2);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CMFCTestTimerView::OnLButtonDown(UINT nFlags, CPoint point)
{
	SetTimer(1, 10, NULL);
	SetTimer(2, 20, NULL);
	CView::OnLButtonDown(nFlags, point);
}


//void CMFCTestTimerView::OnRButtonDown(UINT nFlags, CPoint point)
//{
//	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//	if (!m_bTimerRun)
//	{
//		AfxMessageBox(_T("시계가 이미 동작을 멈추었습니다"));
//	}
//	else
//	{
//		if (AfxMessageBox(_T("시계 동작을 멈추 시겠습니까?"), MB_YESNO | MB_ICONQUESTION) == IDYES)
//		{
//			KillTimer(1);
//			m_bTimerRun = FALSE;
//		}
//	}
//	CView::OnRButtonDown(nFlags, point);
//}


void CMFCTestTimerView::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 1) {
		nX += nMx;
		nY += nMy;

		if (nY + R >= m_Rect.bottom)
		{
			nMy = -STEP;
		}

		if (nX + R >= m_Rect.right)
		{
			nMx = -STEP;
		}

		if (nY - R <= m_Rect.top)
		{
			nMy = STEP;
		}

		if (nX - R <= m_Rect.left)
		{
			nMx = STEP;
		}

		nX2 += nMx2;
		nY2 += nMy2;

		if (nY2 + 20 >= m_Rect.bottom)
		{
			nMy2 = -30;
		}

		if (200 + nX2 + 20 >= m_Rect.right)
		{
			nMx2 = -30;
		}

		if (nY2 - 20 <= m_Rect.top)
		{
			nMy2 = 30;
		}

		if (200 + nX2 - 20 <= m_Rect.left)
		{
			nMx2 = 30;
		}
	}

	Invalidate();
	CView::OnTimer(nIDEvent);
}


void CMFCTestTimerView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	GetClientRect(&m_Rect);
}
