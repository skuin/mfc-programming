﻿
// MFC20190923View.cpp: CMFC20190923View 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MFC20190923.h"
#endif

#include "MFC20190923Doc.h"
#include "MFC20190923View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFC20190923View

IMPLEMENT_DYNCREATE(CMFC20190923View, CView)

BEGIN_MESSAGE_MAP(CMFC20190923View, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_PAINT()
	ON_WM_RBUTTONDOWN()
//	ON_WM_NCLBUTTONDOWN()
ON_WM_LBUTTONDOWN()
ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// CMFC20190923View 생성/소멸

CMFC20190923View::CMFC20190923View() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CMFC20190923View::~CMFC20190923View()
{
}

BOOL CMFC20190923View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CMFC20190923View 그리기

void CMFC20190923View::OnDraw(CDC* /*pDC*/)
{
	CMFC20190923Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CMFC20190923View 인쇄

BOOL CMFC20190923View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMFC20190923View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMFC20190923View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMFC20190923View 진단

#ifdef _DEBUG
void CMFC20190923View::AssertValid() const
{
	CView::AssertValid();
}

void CMFC20190923View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFC20190923Doc* CMFC20190923View::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFC20190923Doc)));
	return (CMFC20190923Doc*)m_pDocument;
}
#endif //_DEBUG


// CMFC20190923View 메시지 처리기


void CMFC20190923View::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CView::OnPaint()을(를) 호출하지 마십시오.
	dc.Rectangle(10, 10, 210, 210);
	dc.Ellipse(220, 10, 420, 210);
}


void CMFC20190923View::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CClientDC dc(this);
	CBrush brush, *oldBrush;
	brush.CreateSolidBrush(m_color);

	oldBrush = dc.SelectObject(&brush);
	dc.Ellipse(point.x - 50, point.y - 50, point.x + 50, point.y + 50);
	dc.SelectObject(oldBrush);
	brush.DeleteObject();

	CView::OnRButtonDown(nFlags, point);
}


void CMFC20190923View::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CClientDC dc(this);
	CPen pen, *oldPen;

	pen.CreatePen(PS_SOLID, 1, m_color);
	oldPen = dc.SelectObject(&pen);
	
	dc.Rectangle(point.x - 50, point.y - 50, point.x + 50, point.y + 50);
	dc.SelectObject(oldPen);
	pen.DeleteObject();
	CView::OnLButtonDown(nFlags, point);
}


void CMFC20190923View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nChar)
	{
	case 'R':
		m_color = RGB(255, 0, 0);
		break;
	case 'G':
		m_color = RGB(0, 255, 0);
		break;
	case 'B':
		m_color = RGB(0, 0, 255);
		break;
	default:
		break;
	}
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
