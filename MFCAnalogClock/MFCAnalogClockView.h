﻿
// MFCAnalogClockView.h: CMFCAnalogClockView 클래스의 인터페이스
//

#pragma once


class CMFCAnalogClockView : public CView
{
protected: // serialization에서만 만들어집니다.
	CMFCAnalogClockView() noexcept;
	DECLARE_DYNCREATE(CMFCAnalogClockView)

// 특성입니다.
public:
	CMFCAnalogClockDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual void OnDraw(CDC* pDC);  // 이 뷰를 그리기 위해 재정의되었습니다.
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CMFCAnalogClockView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()

public:
	int hLen, mLen, sLen; // 시분초침 길이
	int hDegree, mDegree, sDegree; // 시분초 각도
	int h, m, s; // 시분초
	COLORREF m_hColor, m_mColor, m_sColor;

	CString m_strTimeMarker, m_strDate, m_strTime;

	int m_iWatchMode;
	bool m_bStopStart;
	CString m_strStopWatch;
	int m_iSWH, m_iSWM, m_iSWS;

	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // MFCAnalogClockView.cpp의 디버그 버전
inline CMFCAnalogClockDoc* CMFCAnalogClockView::GetDocument() const
   { return reinterpret_cast<CMFCAnalogClockDoc*>(m_pDocument); }
#endif

