﻿
// ExHelloMFCView.cpp: CExHelloMFCView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "ExHelloMFC.h"
#endif

#include "ExHelloMFCDoc.h"
#include "ExHelloMFCView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CExHelloMFCView

IMPLEMENT_DYNCREATE(CExHelloMFCView, CView)

BEGIN_MESSAGE_MAP(CExHelloMFCView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CExHelloMFCView 생성/소멸

CExHelloMFCView::CExHelloMFCView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.

}

CExHelloMFCView::~CExHelloMFCView()
{
}

BOOL CExHelloMFCView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CExHelloMFCView 그리기

void CExHelloMFCView::OnDraw(CDC* pDC)
{
	CExHelloMFCDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
	// 정보출력
	
	// 첫수업 좌표 그리기
	//pDC->TextOut(m_cordinate.x + 10, m_cordinate.y + 10, m_strPoint);

	pDC->TextOut(10, 10, _T("한국산업기술대학교"));
	CRect rect;
	rect.left = 100;
	rect.top = 100;
	rect.right = 300;
	rect.bottom = 200;
	pDC->DrawText(_T("프로그래밍 응용"), &rect, DT_CENTER | DT_VCENTER);
}


// CExHelloMFCView 인쇄

BOOL CExHelloMFCView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CExHelloMFCView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CExHelloMFCView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CExHelloMFCView 진단

#ifdef _DEBUG
void CExHelloMFCView::AssertValid() const
{
	CView::AssertValid();
}

void CExHelloMFCView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CExHelloMFCDoc* CExHelloMFCView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CExHelloMFCDoc)));
	return (CExHelloMFCDoc*)m_pDocument;
}
#endif //_DEBUG


// CExHelloMFCView 메시지 처리기


void CExHelloMFCView::OnMouseMove(UINT nFlags, CPoint point)
{
	// 마우스가 움직일 떄 변화되는 좌표정보를 저장
	//m_str = "한국산업기술대학교";
	m_strPoint.Format(_T("X 좌표:%d, Y좌표 :%d"), m_cordinate.x, m_cordinate.y);
	m_cordinate.x = point.x;
	m_cordinate.y = point.y;
	// Invalidate()라는 함수를 써야 OnDraw()를 호출할 수 있다.
	Invalidate();

	CView::OnMouseMove(nFlags, point);
}


void CExHelloMFCView::OnLButtonDown(UINT nFlags, CPoint point)
{
	AfxMessageBox(_T("오른쪽 버튼클릭"), MB_OK);

	CView::OnLButtonDown(nFlags, point);
}


void CExHelloMFCView::OnRButtonDown(UINT nFlags, CPoint point)
{
	AfxMessageBox(_T("왼쪽 버튼클릭"), MB_OK | MB_ICONINFORMATION);


	CView::OnRButtonDown(nFlags, point);
}
