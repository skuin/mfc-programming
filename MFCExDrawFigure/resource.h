//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MFCExDrawFigure.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_MFCExDrawFigureTYPE         130
#define ID_FREE_LINE                    32772
#define ID_32775                        32775
#define ID_MENU_EXERCISE                32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_LINE                         32780
#define ID_ELLIPSE                      32781
#define ID_RECTANGLE                    32782
#define ID_ACCELERATOR32784             32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        310
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
