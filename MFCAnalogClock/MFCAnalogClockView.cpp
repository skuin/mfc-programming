﻿
// MFCAnalogClockView.cpp: CMFCAnalogClockView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MFCAnalogClock.h"
#endif

#include "MFCAnalogClockDoc.h"
#include "MFCAnalogClockView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <math.h>

#define CENTER_X 300
#define CENTER_Y 300
#define PI 3.141592

// CMFCAnalogClockView

IMPLEMENT_DYNCREATE(CMFCAnalogClockView, CView)

BEGIN_MESSAGE_MAP(CMFCAnalogClockView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

// CMFCAnalogClockView 생성/소멸

CMFCAnalogClockView::CMFCAnalogClockView() noexcept
{
	hLen = 100;
	mLen = 140;
	sLen = 180;

	h = m = s = 0;

	hDegree = sDegree = mDegree = 0;

	m_hColor = RGB(255, 0, 0);
	m_mColor = RGB(0, 255, 0);
	m_sColor = RGB(0, 0, 255);
}

CMFCAnalogClockView::~CMFCAnalogClockView()
{
}

BOOL CMFCAnalogClockView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CView::PreCreateWindow(cs);
}

// CMFCAnalogClockView 그리기

void CMFCAnalogClockView::OnDraw(CDC* /*pDC*/)
{
	CMFCAnalogClockDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}


// CMFCAnalogClockView 인쇄

BOOL CMFCAnalogClockView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMFCAnalogClockView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMFCAnalogClockView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMFCAnalogClockView 진단

#ifdef _DEBUG
void CMFCAnalogClockView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCAnalogClockView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCAnalogClockDoc* CMFCAnalogClockView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCAnalogClockDoc)));
	return (CMFCAnalogClockDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCAnalogClockView 메시지 처리기


void CMFCAnalogClockView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CView::OnPaint()을(를) 호출하지 마십시오.

	CPen pen, hPen, mPen, sPen, *oldPen, cPen;
	CFont font, fontDate,fontMain, *oldfont;

	CDC memdc;
	memdc.CreateCompatibleDC(&dc);
	CBitmap bitmap, *oldbitmap;

	bitmap.LoadBitmapW(IDB_BITMAP1);
	oldbitmap = memdc.SelectObject(&bitmap);
	dc.BitBlt(90, 100, 420, 420, &memdc, 0, 0, SRCCOPY);
	
	fontMain.CreateFont(20, 10, 0, 0, 5, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, 0);
	dc.SelectObject(fontMain);
	dc.TextOutW(CENTER_X - 90, CENTER_Y - 240, _T("* 6조 아날로그 시계 *"));
	dc.TextOutW(CENTER_X - 170, CENTER_Y + 240, _T("* 마우스 왼쪽 클릭 : 시계 모드 변경"));
	fontMain.DeleteObject();


	if (m_iWatchMode == 0) {
		// 시계 테두리(바깥쪽)
		cPen.CreatePen(PS_SOLID, 10, RGB(90, 57, 49));
		oldPen = dc.SelectObject(&cPen);
		//dc.Ellipse(90, 90, 510, 510);

		// 시계테두리 안쪽
		pen.CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
		oldPen = dc.SelectObject(&pen);
		//dc.Ellipse(100, 100, 500, 500);

		//테두리 시각 숫자표시
		int x, y, i, j;
		for (i = 1; i <= 60; i++)
		{
			x = 190 * sin(i*PI / 30);
			y = 190 * -cos(i*PI / 30);

			if ((i % 5) == 0) {
				j = i / 5;
				m_strTimeMarker.Format(_T("%d"), j);
				dc.TextOutW(x + 300 - 6, y + 300 - 6, m_strTimeMarker);
			}
		}

		

		if (h > 12) h -= 12;
		if (m > 60) m -= 60;
		if (s > 60) s -= 60;

		// 시분초 각도 변환
		hDegree = h * (360 / 12) - 90;
		mDegree = m * (360 / 60) - 90;
		sDegree = s * (360 / 60) - 90;

		hPen.CreatePen(PS_SOLID, 6, m_hColor);
		mPen.CreatePen(PS_SOLID, 4, m_mColor);
		sPen.CreatePen(PS_SOLID, 2, m_sColor);

		//시침 그리기
		int hX = CENTER_X + cos((double)hDegree*PI / 180.0) * hLen;
		int hY = CENTER_Y + sin((double)hDegree*PI / 180.0) * hLen;
		oldPen = dc.SelectObject(&hPen);
		dc.MoveTo(CENTER_X, CENTER_Y);
		dc.LineTo(hX, hY);


		//분침 그리기
		int mX = CENTER_X + cos((double)mDegree*PI / 180.0) * mLen;
		int mY = CENTER_Y + sin((double)mDegree*PI / 180.0) * mLen;
		oldPen = dc.SelectObject(&mPen);
		dc.MoveTo(CENTER_X, CENTER_Y);
		dc.LineTo(mX, mY);

		//초침 그리기
		int sX = CENTER_X + cos((double)sDegree*PI / 180.0) * sLen;
		int sY = CENTER_Y + sin((double)sDegree*PI / 180.0) * sLen;
		oldPen = dc.SelectObject(&sPen);
		dc.MoveTo(CENTER_X, CENTER_Y);
		dc.LineTo(sX, sY);

		dc.SelectObject(oldPen);
		pen.DeleteObject();
		hPen.DeleteObject();
		mPen.DeleteObject();
		sPen.DeleteObject();
	}
	if(m_iWatchMode == 1)	//스탑워치 구현
	{
		dc.TextOutW(CENTER_X-170, CENTER_Y + 270, _T("* 마우스 우클릭 : 스탑워치 Start, Stop"));
		font.CreateFont(40, 20, 0, 0, 5, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, 0);
		dc.SelectObject(font);
		pen.CreatePen(PS_SOLID, 2, RGB(255, 0, 125));
		oldPen = dc.SelectObject(&pen);
		m_strStopWatch.Format(_T("%02d  :  %02d  :  %02d"), m_iSWH, m_iSWM, m_iSWS);
		dc.TextOutW(CENTER_X- 130, CENTER_Y, m_strStopWatch);
		dc.SelectObject(oldPen);
		pen.DeleteObject();
		font.DeleteObject();
	}
	if (m_iWatchMode == 2)
	{
		fontDate.CreateFont(40, 20, 0, 0, 5, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, 0);
		dc.SelectObject(fontDate);
		pen.CreatePen(PS_SOLID, 2, RGB(255, 0, 125));
		oldPen = dc.SelectObject(&pen);
		// 날짜, 시간표시
		dc.TextOutW(CENTER_X - 160, CENTER_Y-20, m_strDate);
		dc.TextOutW(CENTER_X - 140, CENTER_Y+20, m_strTime);
		dc.SelectObject(oldPen);
		pen.DeleteObject();
		fontDate.DeleteObject();
	}
}



void CMFCAnalogClockView::OnTimer(UINT_PTR nIDEvent)
{

	// 타이머 시작시에 현재 시각과 날짜를 받아온다.
	COleDateTime time;
	time = COleDateTime::GetCurrentTime();
	h = time.GetHour();
	m = time.GetMinute();
	s = time.GetSecond();
	m_strDate.Format(_T("%d년 %d월 %d일"), time.GetYear(), time.GetMonth(), time.GetDay());
	m_strTime.Format(_T("%d시 %d분 %d초"), h, m, s);
	if (nIDEvent == 0)
	{
	}
	if (nIDEvent == 1)
	{
		if ((m_iWatchMode == 1) && (m_bStopStart == true))
		{
			if (m_iSWS == 60)
			{
				++m_iSWM;
				m_iSWS = 0;
			}
			if (m_iSWM == 60)
			{
				++m_iSWH;
				m_iSWM = 0;
			}
			if (m_iSWH == 60)
			{
				m_iSWH = 0;
				m_iSWS = 0;
				m_iSWM = 0;
			}
			++m_iSWS;
			Invalidate();
		}
	}
	Invalidate();

	CView::OnTimer(nIDEvent);
}


int CMFCAnalogClockView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_iWatchMode = 0;
	m_bStopStart = false;
	m_iSWH = 0;
	m_iSWS = 0;
	m_iSWM = 0;
	
	SetTimer(0, 1000, NULL);
	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	return 0;
}


void CMFCAnalogClockView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// 모드를 변경하는 버튼
	if (m_iWatchMode == 2)
	{
		m_iWatchMode = 0;
	}
	else
	{
		m_iWatchMode++;
	}
	Invalidate();
	CView::OnLButtonDown(nFlags, point);
}


void CMFCAnalogClockView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// 스탑워치 시작, 정지
	if (m_iWatchMode == 1)
	{
		if (m_bStopStart == false)
		{
			if (AfxMessageBox(_T("스톱워치를 동작 시키겠습니까?"), MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				KillTimer(0);
				SetTimer(1, 1000, NULL);
				m_bStopStart = true;
			}
		}
		else
		{
			if (AfxMessageBox(_T("스톱워치를 중지 시키겠습니까?"), MB_YESNO | MB_ICONQUESTION) == IDYES)
			{
				KillTimer(1);
				SetTimer(0, 1000, NULL);
				m_bStopStart = false;
			}
		}
	}
	CView::OnRButtonDown(nFlags, point);
}
