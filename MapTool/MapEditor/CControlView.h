﻿#pragma once

#include "MapEditorView.h"
// 탭1(타일), 탭2(지형)만 사용
class CTap1;
class CTap2;
//class CTap3;

// CControlView 보기

// SDI에 Dialog를 이용하기 위해 폼뷰를 생성
class CControlView : public CFormView
{
	DECLARE_DYNCREATE(CControlView)

protected:
	CControlView(CWnd* pParent =nullptr);           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CControlView();
	void CControlView::DoDataExchange(CDataExchange* pDX);
public:
	virtual void OnDraw(CDC* pDC);      // 이 뷰를 그리기 위해 재정의되었습니다.
	CTap1 *pDlg1;						// ControlView에 Tap Control1을 사용하기 위해 지정
	CTap2 *pDlg2;						// ControlView에 Tap Control2를 사용하기 위해 지정

	bool bClick = false;
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#ifndef AFX_DESIGN_TIME
	enum { IDD = IDD_FORMVIEW };
#endif
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	CModalDialog *pDlg;
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	CTabCtrl m_Tap;						// Tap Control 컨트롤 변수
	virtual void OnInitialUpdate();
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
};


