﻿// MapEditorView.cpp: CMapEditorView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MapEditor.h"
#endif
#include "MainFrm.h"
#include "MapEditorDoc.h"
#include "MapEditorView.h"
#include "CTap1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMapEditorView

IMPLEMENT_DYNCREATE(CMapEditorView, CView)

BEGIN_MESSAGE_MAP(CMapEditorView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CMapEditorView 생성/소멸

CMapEditorView::CMapEditorView() noexcept
{
	// TODO: 여기에 생성 코드를 추가합니다.
	cnt = 0;				//타일 아이콘의 갯수 초기화
	MouseDown = FALSE;
}

CMapEditorView::~CMapEditorView()
{
}

BOOL CMapEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.
	return CView::PreCreateWindow(cs);
}

// CMapEditorView 그리기
void CMapEditorView::OnDraw(CDC* pDC)
{
	CMapEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	GetClientRect(winrect);			// 현재 클라이언트의 크기를 CRect형태로 얻어옴

	pDC->Rectangle(10, 10, winrect.Width(), winrect.Height());		// 만들 맵의 테두리를 그림

	winRatio.x = (winrect.Width() / 30);							// 가로 30개의 타일 아이콘을 그리기 위해 타일 하나당 가로 크기
	winRatio.y = (winrect.Height() / 20);							// 세로 20개의 타일 아이콘을 그리기 위해 타일 하나당 세로 크기


	/*  타일 비율 확인용 메시지
	CString tt;
	tt.Format(_T("%d %d"), winRatio.x, winRatio.y);
	AfxMessageBox(tt);
	*/
	CClientDC dc(this);

	if (m_bStartflag == false) {
		// 비율에 맞게 타일의 세로 선 20개를 그림 
		for (int y = 0; y < winrect.Height() / winRatio.y; y++)
		{
			pDC->MoveTo(winRatio.x, winRatio.y + y * winRatio.y);
			pDC->LineTo(((winrect.Width() / winRatio.x) * winRatio.x), winRatio.y + y * winRatio.y);
		}
		// 비율에 맞게 타일의 가로 선 30개를 그림
		for (int x = 0; x < winrect.Width() / winRatio.x; x++)
		{
			pDC->MoveTo(winRatio.x + x * winRatio.x, winRatio.y);
			pDC->LineTo(winRatio.x + x * winRatio.x, (winrect.Height() / winRatio.y) * winRatio.y);
		}

	}
	else {

		for (int y = 0; y < winrect.Height() / winRatio.y; y++)
		{
			if (y == 0 || y == (winrect.Height() / winRatio.y - 1)) {
				pDC->MoveTo(winRatio.x, winRatio.y + y * winRatio.y);
				pDC->LineTo(((winrect.Width() / winRatio.x) * winRatio.x), winRatio.y + y * winRatio.y);
			}
		}
		// 비율에 맞게 타일의 가로 선 30개를 그림
		for (int x = 0; x < winrect.Width() / winRatio.x; x++)
		{
			if (x == 0 || x == (winrect.Width() / winRatio.x - 1)) {
				pDC->MoveTo(winRatio.x + x * winRatio.x, winRatio.y);
				pDC->LineTo(winRatio.x + x * winRatio.x, (winrect.Height() / winRatio.y) * winRatio.y);
			}
		}

		BITMAP bmpInfo;

		CDC MemDC;
		MemDC.CreateCompatibleDC(&dc);	// CPaintDC와 호환되는 DC의 핸들을 MemDC에 저장

		CBitmap bmp[599];					// 뿌려줄 비트맵 클래스 선언
		CBitmap *pOldbmp[599];


		for (int i = 0; i < cnt; i++) {
			if (KindsofIcon[i] == 1) {
				bmp[i].LoadBitmapW(IDB_BITMAP_TILE1);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));
			}
			else if (KindsofIcon[i] == 2) {
				bmp[i].LoadBitmapW(IDB_BITMAP_TILE2);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 3) {
				bmp[i].LoadBitmapW(IDB_BITMAP_TILE3);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 4) {
				bmp[i].LoadBitmapW(IDB_BITMAP_TILE4);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 5) {
				bmp[i].LoadBitmapW(IDB_BITMAP_TILE5);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 6) {
				bmp[i].LoadBitmapW(IDB_BITMAP_TILE6);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 7) {
				bmp[i].LoadBitmapW(IDB_RAND1);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 8) {
				bmp[i].LoadBitmapW(IDB_RAND2);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 9) {
				bmp[i].LoadBitmapW(IDB_RAND3);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 10) {
				bmp[i].LoadBitmapW(IDB_RAND4);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 11) {
				bmp[i].LoadBitmapW(IDB_RAND5);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 12) {
				bmp[i].LoadBitmapW(IDB_RAND6);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 13) {
				bmp[i].LoadBitmapW(IDB_RAND7);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 14) {
				bmp[i].LoadBitmapW(IDB_RAND8);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 15) {
				bmp[i].LoadBitmapW(IDB_RAND9);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 16) {
				bmp[i].LoadBitmapW(IDB_RAND10);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 17) {
				bmp[i].LoadBitmapW(IDB_RAND11);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 18) {
				bmp[i].LoadBitmapW(IDB_RAND12);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 19) {
				bmp[i].LoadBitmapW(IDB_RAND13);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 20) {
				bmp[i].LoadBitmapW(IDB_RAND14);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 21) {
				bmp[i].LoadBitmapW(IDB_RAND15);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 22) {
				bmp[i].LoadBitmapW(IDB_RAND16);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 23) {
				bmp[i].LoadBitmapW(IDB_RAND17);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 24) {
				bmp[i].LoadBitmapW(IDB_RAND18);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 25) {
				bmp[i].LoadBitmapW(IDB_RAND19);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

			}
			else if (KindsofIcon[i] == 26) {
				bmp[i].LoadBitmapW(IDB_RAND20);
				bmp[i].GetBitmap(&bmpInfo);
				pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
				pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));
			}
		}

		CPen pen(PS_SOLID, 2, RGB(0, 0, 0));
		CPen* pOldPen = dc.SelectObject(&pen);
		CBrush NewBrush(RGB(255, 0, 0));
		CBrush* pOldBrush = dc.SelectObject(&NewBrush);
		// 움직이는 원
		dc.Ellipse(x1, y1, x2, y2);

		dc.SelectObject(pOldPen);
		dc.SelectObject(pOldBrush);

	}

	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.
}

// CMapEditorView 인쇄

BOOL CMapEditorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CMapEditorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CMapEditorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CMapEditorView 진단

#ifdef _DEBUG
void CMapEditorView::AssertValid() const
{
	CView::AssertValid();
}

void CMapEditorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMapEditorDoc* CMapEditorView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMapEditorDoc)));
	return (CMapEditorDoc*)m_pDocument;
}
#endif //_DEBUG


// 마우스가 이동 윈도우 메시지가 발생할 때 호출
void CMapEditorView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CPaintDC dc(this);

	// 왼쪽 마우스가 클릭되고 마우스가 이동할 때
	if (MouseDown == TRUE)
	{
		int ax, ay;

		ax = point.x / winRatio.x;	// 마우스 커서의 x축 위치를 타일 하나의 가로 크기로 나눠줌
		ay = point.y / winRatio.y;	// 마우스 커서의 y축 위치를 타일 하나의 세로 크기로 나눠줌

		if ((point.x < winRatio.x) || (ax >= winrect.Width() / winRatio.x) || (point.y < winRatio.y) || (ay >= winrect.Height() / winRatio.y))
			return;

		CClientDC dc(this);
		DrawIcon(&dc, ax, ay);		// 해당 dc에 타일 아이콘을 그려줌
	}

	CView::OnMouseMove(nFlags, point);
}

// 왼쪽 마우스 클릭 윈도우 메시지가 발생할 때 호출
void CMapEditorView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	int ax, ay;

	ax = point.x / winRatio.x;		// 마우스 커서의 x축 위치를 타일 하나의 가로 크기로 나눠줌
	ay = point.y / winRatio.y;		// 마우스 커서의 y축 위치를 타일 하나의 세로 크기로 나눠줌

	if ((point.x < winRatio.x) || (ax >= winrect.Width() / winRatio.x) || (point.y < winRatio.y) || (ay >= winrect.Height() / winRatio.y))
		return;

	//CString ms;
	//ms.Format(_T("위치 x = %d, y = %d"), point.x, point.y);
	//AfxMessageBox(ms);

	CClientDC dc(this);
	DrawIcon(&dc, ax, ay);			// 해당 dc에 타일 아이콘을 그려줌
	MouseDown = TRUE;				// 마우스가 클릭 된 것을 알려줌
	CView::OnLButtonDown(nFlags, point);
}

// 왼쪽 마우스 UP 윈도우 메시지가 발생할 때 호출
void CMapEditorView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (MouseDown == TRUE)
		MouseDown = FALSE;
	CView::OnLButtonUp(nFlags, point);
}


void CMapEditorView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
}

// 해당 DC에 타일 아이콘을 그려줌
void CMapEditorView::DrawIcon(CDC* pDC, int x, int y)
{
	CPaintDC dc(this);

	BITMAP bmpInfo;

	CDC MemDC;
	MemDC.CreateCompatibleDC(&dc);	// CPaintDC와 호환되는 DC의 핸들을 MemDC에 저장

	CBitmap bmp;					// 뿌려줄 비트맵 클래스 선언
	CBitmap *pOldbmp;

	// Tap Control에서 얻은 checkbit를 이용하여 원하는 비트맵 선택
	if (m_bStartflag == false) {
		switch (checkbit)
		{
		case 1:
		{
			bmp.LoadBitmapW(IDB_BITMAP_TILE1);

			arrIcon[cnt].x = x;			// 아이콘의 열 값
			arrIcon[cnt].y = y;			// 아이콘의 행 값
			IS_Move[cnt] = 0;			// 해당 아이콘인 타일인지 지형인지
			KindsofIcon[cnt] = 1;		// 어떤 종류의 타일 아이콘인지 규별하기 위한 변수
			cnt++;						// 만든 타일 아이콘의 갯수 증가

			
			break;
		}
		case 2:
		{
			bmp.LoadBitmapW(IDB_BITMAP_TILE2);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 0;
			KindsofIcon[cnt] = 2;
			cnt++;

			break;
		}
		case 3:
		{
			bmp.LoadBitmapW(IDB_BITMAP_TILE3);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 0;
			KindsofIcon[cnt] = 3;
			cnt++;

			break;
		}
		case 4:
		{
			bmp.LoadBitmapW(IDB_BITMAP_TILE4);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 0;
			KindsofIcon[cnt] = 4;
			cnt++;

			break;
		}
		case 5:
		{
			bmp.LoadBitmapW(IDB_BITMAP_TILE5);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 0;
			KindsofIcon[cnt] = 5;
			cnt++;

			break;
		}
		case 6:
		{
			bmp.LoadBitmapW(IDB_BITMAP_TILE6);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 0;
			KindsofIcon[cnt] = 6;
			cnt++;
			break;
		}
		case 7:
		{
			bmp.LoadBitmapW(IDB_RAND1);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 7;
			cnt++;
			break;
		}
		case 8:
		{
			bmp.LoadBitmapW(IDB_RAND2);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 8;
			cnt++;
			break;
		}
		case 9:
		{
			bmp.LoadBitmapW(IDB_RAND3);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 9;
			cnt++;
			break;
		}
		case 10:
		{
			bmp.LoadBitmapW(IDB_RAND4);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 10;
			cnt++;
			break;
		}
		case 11:
		{
			bmp.LoadBitmapW(IDB_RAND5);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 11;
			cnt++;
			break;
		}
		case 12:
		{
			bmp.LoadBitmapW(IDB_RAND6);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 12;
			cnt++;
			break;
		}
		case 13:
		{
			bmp.LoadBitmapW(IDB_RAND7);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 13;
			cnt++;
			break;
		}
		case 14:
		{
			bmp.LoadBitmapW(IDB_RAND8);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 14;
			cnt++;
			break;
		}
		case 15:
		{
			bmp.LoadBitmapW(IDB_RAND9);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 15;
			cnt++;
			break;
		}
		case 16:
		{
			bmp.LoadBitmapW(IDB_RAND10);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 16;
			cnt++;
			break;
		}
		case 17:
		{
			bmp.LoadBitmapW(IDB_RAND11);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 17;
			cnt++;
			break;
		}
		case 18:
		{
			bmp.LoadBitmapW(IDB_RAND12);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 18;
			cnt++;
			break;
		}
		case 19:
		{
			bmp.LoadBitmapW(IDB_RAND13);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 19;
			cnt++;
			break;
		}
		case 20:
		{
			bmp.LoadBitmapW(IDB_RAND14);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 20;
			cnt++;
			break;
		}
		case 21:
		{
			bmp.LoadBitmapW(IDB_RAND15);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 21;
			cnt++;
			break;
		}
		case 22:
		{
			bmp.LoadBitmapW(IDB_RAND16);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 22;
			cnt++;
			break;
		}
		case 23:
		{
			bmp.LoadBitmapW(IDB_RAND17);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 23;
			cnt++;
			break;
		}
		case 24:
		{
			bmp.LoadBitmapW(IDB_RAND18);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 24;
			cnt++;
			break;
		}
		case 25:
		{
			bmp.LoadBitmapW(IDB_RAND19);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 25;
			cnt++;
			break;
		}
		case 26:
		{
			bmp.LoadBitmapW(IDB_RAND20);
			arrIcon[cnt].x = x;
			arrIcon[cnt].y = y;
			IS_Move[cnt] = 1;
			KindsofIcon[cnt] = 26;
			cnt++;
			break;
		}
		default:
			break;
		}

		bmp.GetBitmap(&bmpInfo);			// CBitmap 객체의 비트맵 정보를 BITMAP 구조체에 저장
		pOldbmp = MemDC.SelectObject(&bmp);	// MemDC에 생성된 비트맵 bmp를 연결

		//장애물이 아닐 때
		if (IS_Move[cnt - 1] == 0)
			pDC->StretchBlt(x * winRatio.x, y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, SRCCOPY);	//pDC에 해당 비트맵을 사이즈를 변경해서 출력
		// 장애물 일 때
		else
		{
			// 흰색 배경을 제거하고 출력
			// 비트맵 12, 13, 14는 비트맵의 세로 사이즈가 달라서 따로 설정해둠
			if (KindsofIcon[cnt - 1] <= 14 && KindsofIcon[cnt - 1] >= 12)
				pDC->TransparentBlt(x * winRatio.x, y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));
			else
				pDC->TransparentBlt(x * winRatio.x, y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));
		}
	}
}

void CMapEditorView::DrawMap(CDC * pDC)
{
	CPaintDC dc(this);

	BITMAP bmpInfo;

	CPen pen;
	pen.CreatePen(PS_DOT, 3, RGB(0, 0, 0));    // 빨간색 펜을 생성
	CPen* oldPen = dc.SelectObject(&pen);
	CBrush brush;
	brush.CreateSolidBrush(RGB(0, 0, 0));     // 오렌지색 채움색을 생성
	CBrush* oldBrush = dc.SelectObject(&brush);

	CDC MemDC;
	MemDC.CreateCompatibleDC(&dc);	// CPaintDC와 호환되는 DC의 핸들을 MemDC에 저장

	CBitmap bmp[599];					// 뿌려줄 비트맵 클래스 선언
	CBitmap *pOldbmp[599];

	for (int i = 0; i < cnt; i++) {
		if (KindsofIcon[i] == 1) {
			bmp[i].LoadBitmapW(IDB_BITMAP_TILE1);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));
		}
		else if (KindsofIcon[i] == 2) {
			bmp[i].LoadBitmapW(IDB_BITMAP_TILE2);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 3) {
			bmp[i].LoadBitmapW(IDB_BITMAP_TILE3);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 4) {
			bmp[i].LoadBitmapW(IDB_BITMAP_TILE4);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 5) {
			bmp[i].LoadBitmapW(IDB_BITMAP_TILE5);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 6) {
			bmp[i].LoadBitmapW(IDB_BITMAP_TILE6);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 7) {
			bmp[i].LoadBitmapW(IDB_RAND1);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 8) {
			bmp[i].LoadBitmapW(IDB_RAND2);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 9) {
			bmp[i].LoadBitmapW(IDB_RAND3);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 10) {
			bmp[i].LoadBitmapW(IDB_RAND4);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 11) {
			bmp[i].LoadBitmapW(IDB_RAND5);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 12) {
			bmp[i].LoadBitmapW(IDB_RAND6);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 13) {
			bmp[i].LoadBitmapW(IDB_RAND7);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 14) {
			bmp[i].LoadBitmapW(IDB_RAND8);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 22, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 15) {
			bmp[i].LoadBitmapW(IDB_RAND9);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 16) {
			bmp[i].LoadBitmapW(IDB_RAND10);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 17) {
			bmp[i].LoadBitmapW(IDB_RAND11);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 18) {
			bmp[i].LoadBitmapW(IDB_RAND12);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 19) {
			bmp[i].LoadBitmapW(IDB_RAND13);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 20) {
			bmp[i].LoadBitmapW(IDB_RAND14);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 21) {
			bmp[i].LoadBitmapW(IDB_RAND15);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 22) {
			bmp[i].LoadBitmapW(IDB_RAND16);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 23) {
			bmp[i].LoadBitmapW(IDB_RAND17);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 24) {
			bmp[i].LoadBitmapW(IDB_RAND18);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 25) {
			bmp[i].LoadBitmapW(IDB_RAND19);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));

		}
		else if (KindsofIcon[i] == 26) {
			bmp[i].LoadBitmapW(IDB_RAND20);
			bmp[i].GetBitmap(&bmpInfo);
			pOldbmp[i] = MemDC.SelectObject(&bmp[i]);
			pDC->TransparentBlt(arrIcon[i].x * winRatio.x, arrIcon[i].y * winRatio.y, winRatio.x, winRatio.y, &MemDC, 0, 0, 30, 30, RGB(255, 255, 255));
		}
	}
}


void CMapEditorView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CClientDC dc(this);

	switch (nChar)
	{
	case VK_RETURN:
		AfxMessageBox(_T("게임시작"));
		m_bStartflag = true;
		OnDraw(&dc);
		//SetTimer(2, 100, NULL);
		break;
	case VK_SPACE:
		//SetTimer(1, 100, NULL);
		//Invalidate();
		break;
	case VK_LEFT:
		x1 -= SPEED;
		x2 -= SPEED;
		Invalidate();
		break;
	case VK_RIGHT:
		x1 += SPEED;
		x2 += SPEED;
		Invalidate();
		break;
	case VK_UP:
		y1 -= SPEED;
		y2 -= SPEED;
		Invalidate();
		break;
	case VK_DOWN:
		y1 += SPEED;
		y2 += SPEED;
		Invalidate();
		break;
	}
	
	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CMapEditorView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case 1:
		break;
	case 2:
		break;
	default:
		break;
	}


	CView::OnTimer(nIDEvent);
}
